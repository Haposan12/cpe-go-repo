package models

type GetContentResponse struct {
	Type 		string		`json:"type"`
	Title		string		`json:"title"`
	Description	string		`json:"description"`
	Url 		string		`json:"url"`
}
