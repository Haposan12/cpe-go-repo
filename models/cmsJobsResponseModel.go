package models

type GetJobCategoryResponse struct {
	ID 			int		`json:"id"`
	Name		string	`json:"category_name"`
	Status		int		`json:"active"`
	CreatedAt	string	`json:"created_at"`
}

type GetJobListingResponse struct {
	ID 				int		`json:"id"`
	JobCategoryId	int		`json:"job_category_id"`
	Title			string	`json:"job_title"`
	Description		string	`json:"job_description"`
	JobFunction 	string	`json:"job_function"`
	JobType			int		`json:"job_type"`
	TotalVacancies	int		`json:"total_vacancies"`
	Status			int		`json:"active"`
	CreatedAt		string	`json:"created_at"`
	CategoryName	string	`json:"category_name"`
}


