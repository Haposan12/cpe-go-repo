package models


type CmsUserLoginResponse struct{
	Username	string	`json:"username"`
	Token		string	`json:"token"`
	Fullname	string	`json:"fullname"`
	Role 		string	`json:"role"`
	Type 		string	`json:"user"`
}
