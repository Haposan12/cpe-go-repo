package models

type GetPressMediaResponse struct {
	ID				int		`json:"id"`
	Name 			string	`json:"name"`
	AssetRootLink	string	`json:"asset_root_link"`
	AssetMediaLink	string	`json:"asset_media_link"`
	CreatedAt		string	`json:"created_at"`
	StatusActive	int		`json:"active"`
}

type GetPressArticleResponse struct {
	ID				int		`json:"id"`
	MediaID			int		`json:"media_id"`
	Title			string	`json:"title"`
	Link 			string	`json:"href_link"`
	Published		string	`json:"date_published"`
	CreatedAt		string 	`json:"created_at"`
	Status			int 	`json:"active"`
	Name 			string	`json:"name"`
	AssetRootLink	string	`json:"asset_root_link"`
	AssetMediaLink	string	`json:"asset_media_link"`
}

type GetPressReleaseResponse struct {
	ID 				int		`json:"id"`
	Title			string	`json:"title"`
	Body			string	`json:"body"`
	Status			int 	`json:"active"`
	CreatedAt		string	`json:"created_at"`
	UpdatedAt 		string	`json:"updated_at"`
	CreatedBy		string	`json:"created_by"`
	UpdatedBy		string	`json:"updated_by"`
}
