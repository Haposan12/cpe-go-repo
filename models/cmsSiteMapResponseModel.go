package models

type GetSiteMapResponseModel struct {
	ID 				int 		`json:"id"`
	Location 		string		`json:"location"`
	LastModified	string		`json:"last_modified"`
	ChangeFreq		string		`json:"changefreq"`
	Priority		float64		`json:"priority"`
}
