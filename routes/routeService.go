package routes

import (
	"cpe-go-repo/constants"
	"cpe-go-repo/controllers"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"net/http"
)

func NewRoutes() http.Handler  {
	r := chi.NewRouter()
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-Requested-With", "access-token", "initial-code"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
	})
	r.Use(cors.Handler)
	r.Use(middleware.Throttle(constants.MaxRequest))
	r.Use(middleware.Logger)
	registerRoutes(r)
	return http.TimeoutHandler(r, constants.TimeoutDuration, `{"Message": "Service Unavailable"}`)
}

func registerRoutes(r *chi.Mux) *chi.Mux  {
	r.Group(func(r chi.Router) {
		r.Get("/ping", controllers.Ping)
	})

	r.Group(func(r chi.Router) {
		r.Route("/cms", func(r chi.Router) {
			//r.Use(basicAuthMiddleware)
			//cms user
			r.Post("/user/login", controllers.CmsUserLogin)


			//cms press_media
			//r.Post("/press_media", controllers.InsertPressMedia)
			r.Get("/press_media", controllers.GetPressMedia)
			r.Delete("/press_media", controllers.DeletePressMedia)

			//cms press_media article
			r.Post("/press_media/article", controllers.InsertPressArticle)
			r.Get("/press_media/article", controllers.GetPressArticle)
			r.Delete("/press_media/article", controllers.DeletePressArticle)
			r.Put("/press_media/article", controllers.UpdatePressArticle)

			//cms press_release
			r.Get("/press_release", controllers.GetPressRelease)
			r.Delete("/press_release", controllers.DeletePressRelease)
			r.Get("/press_release/view", controllers.GetDetailPressRelease)
			r.Post("/press_release", controllers.InsertPressRelease)

			//cms sitemap
			r.Get("/sitemap", controllers.GetSiteMap)
			r.Post("/sitemap", controllers.InsertSiteMap)
			r.Put("/sitemap", controllers.UpdateSiteMap)
			r.Delete("/sitemap", controllers.DeleteSiteMap)

			//cms job categories
			r.Post("/job/categories", controllers.InsertJobCategories)
			r.Get("/job/categories", controllers.GetJobCategories)

			//cms job listing
			r.Post("/job/listings", controllers.InsertJobListing)
			r.Get("/job/listings", controllers.GetJobListing)
			r.Get("/job/listings/{id}", controllers.GetJobDetail)
			//r.Get("/job/listings/{id}/applicants", controllers.GetApplicantList)
			r.Put("/job/listings/{id}", controllers.UpdateJobListing)

			//cms content
			r.Post("/content", controllers.AddContent)
			r.Delete("/content/{id}", controllers.DeleteContent)

			//cms counter dashboard
			//r.Get("/counter/dashboard", controllers.GetCountDashboardAdmin)
		})
	})

	return r
}