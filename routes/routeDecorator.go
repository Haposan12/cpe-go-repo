package routes

import (
	"github.com/go-chi/chi"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func FileServer(r chi.Router, path, sourceFile string)  {
	if strings.ContainsAny(path, "{}*"){
		panic("FileServer does not permit URL parameters.")
	}

	workDir, _ := os.Getwd()
	filesDir := filepath.Join(workDir, sourceFile)
	root := http.Dir(filesDir)

	fs := http.StripPrefix(path, http.FileServer(root))

	if path != "/" && path[len(path)-1] != '/'{
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	}))
}
