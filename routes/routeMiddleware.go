package routes

import (
	"bitbucket.org/tunaiku/common-security"
	"bitbucket.org/tunaiku/common-security/http/middlewares/jwt"
	"golang-rest-template/config"
	"golang-rest-template/constants"
	"net/http"
)

// basicAuthMiddleware is a middleware to accept basic auth header
func basicAuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Middleware logic starts here
		/* _, _, ok := r.BasicAuth()
		if !ok {
			response.HTTPResponse(w, "FORBIDDEN", http.StatusForbidden)
			return
		} */
		// Middleware logic ends here
		next.ServeHTTP(w, r)
	})
}

func keyCloakJwksOpenidCertsMiddleware() jwt.Middleware {
	uri := config.EnvConfig.KeyCloak.BaseAuthUrl + constants.JwksValidationUrl
	JwksValidationMiddleware := commonsecurity.JwksValidationMiddleware(commonsecurity.JwksValidationMiddlewareOpts().WithDefaultOpts().SetJwksUrl(uri).AddWhitelist("/*").Build())
	return JwksValidationMiddleware
}
