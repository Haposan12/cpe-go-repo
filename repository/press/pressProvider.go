package press

import (
	"context"
	"cpe-go-repo/config"
	"cpe-go-repo/models"
	"cpe-go-repo/request"
)

func GetPressMedia(ctx context.Context)(data []models.GetPressMediaResponse, err error)  {
	var article models.GetPressMediaResponse
	query := `SELECT * from public.master_media where active=1`

	rs, err := config.GlobalDbSQL.QueryContext(ctx, query)

	defer rs.Close()
	for rs.Next(){
		rs.Scan(&article.ID, &article.Name, &article.AssetRootLink, &article.AssetMediaLink, &article.CreatedAt, &article.StatusActive,)
		data = append(data, article)
	}

	return
}

func DeletePressMedia(ctx context.Context, id string)(err error)  {
	query := `UPDATE public.master_media set active=0 where id=$1`

	_, err = config.GlobalDbSQL.QueryContext(ctx, query, id)

	return
}

func InsertPressArticle(ctx context.Context, rq request.InsertPressArticleRequest)(data models.GetPressArticleResponse, err error)  {
	var articleID int
	query := `INSERT INTO public.press_article (media_id, title, href_link, date_published)
			VALUES ($1, $2, $3, $4) RETURNING id`

	err = config.GlobalDbSQL.QueryRow(query, rq.MediaID, rq.Title, rq.Link, rq.Published).Scan(&articleID)
	data, err = GetPressArticleById(ctx, articleID)
	return
}

func GetPressArticleById(ctx context.Context, id int) (data models.GetPressArticleResponse, err error) {
	query := `select press_article.*, master_media.name, master_media.asset_media_link, master_media.asset_root_link 
			from press_article INNER JOIN master_media 
			on press_article.media_id = master_media.id 
			where press_article.active=1 and press_article.id=$1
			ORDER BY press_article.created_at desc`

	err = config.GlobalDbSQL.QueryRowContext(ctx, query, id).Scan(
		&data.ID,
		&data.MediaID,
		&data.Title,
		&data.Link,
		&data.Published,
		&data.CreatedAt,
		&data.Status,
		&data.Name,
		&data.AssetMediaLink,
		&data.AssetRootLink)

	return
}

func GetPressArticle(ctx context.Context, limit int, offset int, show string) (data []models.GetPressArticleResponse, err error) {
	var article models.GetPressArticleResponse

	query := `select press_article.*, master_media.name, master_media.asset_media_link, master_media.asset_root_link from press_article INNER JOIN master_media on press_article.media_id = master_media.id where press_article.active=1 ORDER BY press_article.created_at desc`
	rs, err := config.GlobalDbSQL.QueryContext(ctx, query)

	defer rs.Close()
	var i = 1
	for rs.Next(){
		rs.Scan(
			&article.ID,
			&article.MediaID,
			&article.Title,
			&article.Link,
			&article.Published,
			&article.CreatedAt,
			&article.Status,
			&article.Name,
			&article.AssetMediaLink,
			&article.AssetRootLink,
		)

		if (i >= offset) && (i <= limit) && (show == "limit"){
			data = append(data, article)
		}else if(show != "limit"){
			data = append(data, article)
		}
		i++
	}

	return
}

func DeletePressArticle(ctx context.Context, id string)(err error)  {
	query := `UPDATE public.press_article set active=0 where id=$1`

	_, err = config.GlobalDbSQL.QueryContext(ctx, query, id)

	return
}

func UpdatePressArticle(ctx context.Context, rq request.UpdatePressArticleRequest)(data models.GetPressArticleResponse, err error)  {
	query := `UPDATE public.press_article
			SET media_id=$1, title=$2, href_link=$3, date_published=$4
			WHERE id=$5`

	_, err = config.GlobalDbSQL.QueryContext(ctx, query, rq.MediaID, rq.Title, rq.Link, rq.Published, rq.ID)

	data, err = GetPressArticleById(ctx, rq.ID)
	return
}

func InsertPressRelease(ctx context.Context, rq request.InsertPressReleaseRequest)(err error)  {
	query := `INSERT INTO press_release (title, body, created_by, updated_by)
			VALUES ($1,$2,$3,$4)`

	_, err = config.GlobalDbSQL.QueryContext(ctx, query, rq.Title, rq.Body, rq.User, rq.User)
	return
}

func GetPressRelease(ctx context.Context, limit, offset int, show string) (data []models.GetPressReleaseResponse, err error) {
	var article models.GetPressReleaseResponse

	query := `SELECT * from press_release where active=1`

	rs, err := config.GlobalDbSQL.QueryContext(ctx, query)

	defer rs.Close()
	var i = 1
	for rs.Next(){
		 rs.Scan(
			&article.ID,
			&article.Title,
			&article.Body,
			&article.Status,
			&article.CreatedAt,
			&article.UpdatedAt,
			&article.CreatedBy,
			&article.UpdatedBy,
		)

		if (i >= offset) && (i <= limit) && (show == "limit"){
			data = append(data, article)
		}else if show != "limit"{
			data = append(data, article)
		}
		i++
	}

	return
}

func DeletePressRelease(ctx context.Context, id string)(err error)  {
	query := `UPDATE public.press_release set active=0 where id=$1`

	_, err = config.GlobalDbSQL.QueryContext(ctx, query, id)

	return
}

func GetDetailPressRelease(ctx context.Context, id string) (data models.GetPressReleaseResponse, err error)  {
	query := `SELECT * from press_release where active=1 and id=$1`

	err = config.GlobalDbSQL.QueryRowContext(ctx, query, id).Scan(
		&data.ID,
		&data.Title,
		&data.Body,
		&data.Status,
		&data.CreatedAt,
		&data.UpdatedAt,
		&data.CreatedBy,
		&data.UpdatedBy,)

	return
}
