package cmsContent

import (
	"context"
	"cpe-go-repo/config"
	"cpe-go-repo/request"
)

func InsertContent(ctx context.Context, rq request.InsertContentRequest)(err error)  {
	query := `INSERT INTO content (type, title, description, url) values($1, $2, $3, $4)`

	_, err = config.GlobalDbSQL.QueryContext(ctx, query, rq.ContentType, rq.Title, rq.Description, rq.Url)
	return
}
//
//func GetContent(ctx context.Context)(data []models.GetContentResponse, err error)  {
//
//}

func DeleteContent(ctx context.Context, id string)(err error)  {
	query := `UPDATE public.content SET active=false WHERE id=$1`

	_, err = config.GlobalDbSQL.QueryContext(ctx, query, id)
	return
}
