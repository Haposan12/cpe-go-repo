package cms

import (
	"context"
	"cpe-go-repo/config"
	"cpe-go-repo/models"
)

//func Register(data *cms.CmsUser)error  {
//	statement := `INSERT INTO users.users(username, password, full_name, role, usertype) values ($1, $2, $3, $4, $5)`
//	query := utils.Query{
//		Statement: statement,
//		Args:      []interface{}{data.Username, data.Password, data.FullName, data.Role, data.Type},
//	}
//	//_, err := utils.QueryOne(config.GlobalDbSQL, query)
//	_, err := utils.ExecSQL(config.GlobalDbSQL, query)
//	if err != nil{
//		return err
//	}
//
//	return nil
//
//}

func CmsUserLogin(ctx context.Context, username, password string)(data models.CmsUserLoginResponse, err error)  {
	query := `SELECT username, full_name, role, usertype from users.users where username = $1 and password = $2`

	err = config.GlobalDbSQL.QueryRowContext(ctx, query, username, password).Scan(&data.Username, &data.Fullname, &data.Role, &data.Type)

	return
}

//func GetUsers(ctx context.Context)([]cms.CmsUser, error)  {
//	var users []cms.CmsUser
//	var data cms.CmsUser
//	query := `SELECT id, username, created_at, role, full_name from users.users`
//	rs, err := config.GlobalDbSQL.QueryContext(ctx, query)
//
//	if err != nil{
//		return users, err
//	}
//	defer rs.Close()
//	for rs.Next(){
//		rs.Scan(&data.ID, &data.Username, &data.CreatedAt, &data.Role, &data.FullName)
//		users = append(users, data)
//	}
//	return users, nil
//}