package job

import (
	"context"
	"cpe-go-repo/config"
	"cpe-go-repo/models"
	"cpe-go-repo/request"
	"cpe-go-repo/utils"
)

func InsertJobCategory(ctx context.Context, rq request.InsertJobCategoriesRequest) (response models.GetJobCategoryResponse, err error)  {
	var categoryID int
	query := `INSERT INTO jobs.job_category (category_name) VALUES ($1) RETURNING id`

	err = config.GlobalDbSQL.QueryRow(query, rq.Name).Scan(&categoryID)

	response, err = GetJobCategoryById(ctx, categoryID)
	return
}

func GetJobCategoryById(ctx context.Context, id int) (data models.GetJobCategoryResponse, err error)  {
	query := `SELECT * from jobs.job_category where id=$1`

	err = config.GlobalDbSQL.QueryRowContext(ctx, query, id).Scan(&data.ID, &data.Name, &data.Status, &data.CreatedAt)
	return
}

func GetJobCategories(ctx context.Context) (data []models.GetJobCategoryResponse, err error) {
	var category models.GetJobCategoryResponse
	query := `SELECT * from jobs.job_category where active=1`

	rs, err := config.GlobalDbSQL.QueryContext(ctx, query)
	defer rs.Close()

	for rs.Next(){
		err = rs.Scan(
			&category.ID,
			&category.Name,
			&category.Status,
			&category.CreatedAt,
		)
		data = append(data, category)
	}
	return
}

func InsertJobListing(ctx context.Context, rq request.InsertJobListingRequest) (response models.GetJobListingResponse, err error)  {
	var jobListingID int

	query := `insert into jobs.job_listing (job_category_id, job_title, job_description, job_function, job_type, total_vacancies) values ($1, $2, $3, $4, $5, $6) returning id`

	err = config.GlobalDbSQL.QueryRow(query, rq.ID, rq.Tittle, rq.Description, rq.Function, rq.Type, rq.Vacancies).Scan(&jobListingID)
	response, err = GetJobListingById(ctx, jobListingID)
	return
}

func GetJobListingById(ctx context.Context, id int) (data models.GetJobListingResponse, err error)  {
	query := `SELECT * from jobs.job_listing where id=$1`

	err = config.GlobalDbSQL.QueryRowContext(ctx, query, id).Scan(
		&data.ID,
		&data.JobCategoryId,
		&data.Title,
		&data.Description,
		&data.JobFunction,
		&data.JobType,
		&data.TotalVacancies,
		&data.Status,
		&data.CreatedAt)

	return
}

func GetJobListing(ctx context.Context, id , status string)(data []models.GetJobListingResponse, err error)  {
	var jobListing models.GetJobListingResponse

	query := `SELECT t1.*, t2.category_name
			FROM jobs.job_listing t1 INNER JOIN jobs.job_category t2
			ON t2.id = t1.job_category_id`

	rs, err := config.GlobalDbSQL.QueryContext(ctx, query)
	for rs.Next(){
		rs.Scan(
			&jobListing.ID,
			&jobListing.JobCategoryId,
			&jobListing.Title,
			&jobListing.Description,
			&jobListing.JobFunction,
			&jobListing.JobType,
			&jobListing.TotalVacancies,
			&jobListing.Status,
			&jobListing.CreatedAt,
			&jobListing.CategoryName)

		if status == "active" && jobListing.Status == 1{
			if id == "0"{
				data = append(data, jobListing)
			}else{
				if utils.StringToInt(id) == jobListing.ID{
					data = append(data, jobListing)
				}
			}
		}else{
			if id == "0"{
				data = append(data, jobListing)
			}else{
				if utils.StringToInt(id) == jobListing.ID{
					data = append(data, jobListing)
				}
			}
		}
	}

	return
}

func GetJobDetail(ctx context.Context, id string)(data models.GetJobListingResponse, err error)  {
	query := `SELECT t1.*, t2.category_name from jobs.job_listing t1 INNER JOIN jobs.job_category t2
			ON t2.id = t1.job_category_id
			WHERE t1.id = $1`

	err = config.GlobalDbSQL.QueryRowContext(ctx, query, id).Scan(
		&data.ID,
		&data.JobCategoryId,
		&data.Title,
		&data.Description,
		&data.JobFunction,
		&data.JobType,
		&data.TotalVacancies,
		&data.Status,
		&data.CreatedAt,
		&data.CategoryName)

	return
}

func GetApplicantList(ctx context.Context, job_id, job_type string)()  {
	
}

func UpdateJobListing(ctx context.Context, rq request.UpdateJobListingRequest)(data models.GetJobListingResponse, err error)  {
	query := `UPDATE jobs.job_listing
			SET job_category_id=$1, job_title=$2, job_description=$3, job_function=$4, job_type=$5, total_vacancies=$6,
			active=$7 WHERE id=$8`

	_, err = config.GlobalDbSQL.QueryContext(ctx, query, rq.CategoryID, rq.Title, rq.Description, rq.JobFunction, rq.JobType, rq.Vacancies, rq.Status, rq.ID)

	data, err = GetJobListingById(ctx, utils.StringToInt(rq.ID))
	return
}