package sitemap

import (
	"context"
	"cpe-go-repo/config"
	"cpe-go-repo/models"
	"cpe-go-repo/request"
	"cpe-go-repo/utils"
)

func GetSiteMap(ctx context.Context)(data []models.GetSiteMapResponseModel, err error)  {
	var site models.GetSiteMapResponseModel
	query := `SELECT * from public.sitemap`

	rs, err := config.GlobalDbSQL.QueryContext(ctx, query)
	for rs.Next(){
		rs.Scan(
			&site.ID,
			&site.Location,
			&site.LastModified,
			&site.ChangeFreq,
			&site.Priority)

		data = append(data, site)
	}
	return
}

func GetSiteMapById(ctx context.Context, id int)(data models.GetSiteMapResponseModel, err error)  {
	query := `SELECT * from public.sitemap WHERE id=$1`

	err = config.GlobalDbSQL.QueryRowContext(ctx, query, id).Scan(&data.ID, &data.Location, &data.LastModified, &data.ChangeFreq, &data.Priority)
	return
}

func InsertSiteMap(ctx context.Context, rq request.InsertSiteMapRequest) (data models.GetSiteMapResponseModel, err error)  {
	var siteID int
	query := `INSERT INTO public.sitemap (location, changefreq, priority) VALUES ($1, $2, $3) RETURNING id`

	err = config.GlobalDbSQL.QueryRow(query, rq.Location, rq.ChangeFreq, rq.Priority).Scan(&siteID)

	data, err = GetSiteMapById(ctx, siteID)
	return
}

func UpdateSiteMap(ctx context.Context, rq request.UpdateSiteMapRequest) (data models.GetSiteMapResponseModel, err error)  {
	query := `UPDATE public.sitemap set location=$1, changefreq=$2, priority=$3, last_modified=$4 where id=$5`

	_, err = config.GlobalDbSQL.Query(query, rq.Location, rq.ChangeFreq, rq.Priority, rq.LastModified, rq.ID)

	data, err = GetSiteMapById(ctx, utils.StringToInt(rq.ID))
	return
}

func DeleteSiteMap(ctx context.Context, id string)(err error)  {
	query := `DELETE FROM public.sitemap where id=$1`

	_, err = config.GlobalDbSQL.QueryContext(ctx, query, id)
	return
}
