package config

import (
	"cpe-go-repo/utils"
	"flag"
	"io/ioutil"
	"path"
	"runtime"

	"gopkg.in/yaml.v2"
)

// EnvConfig initialize for local environment
var EnvConfig Environment

// GetConfigFile func to parse yml config
func GetConfigFile(env *Environment, typeEnv string) error {
	var filepath string
	// Source Path to environment
	_, filename, _, _ := runtime.Caller(1)

	switch typeEnv {
	case "development":
		filepath = path.Join(path.Dir(filename), "../infrastructures/connections/development.yml")

	case "staging":
		filepath = path.Join(path.Dir(filename), "../infrastructures/connections/staging.yml")

	case "production":
		filepath = path.Join(path.Dir(filename), "../infrastructures/connections/production.yml")

	default:
		filepath = path.Join(path.Dir(filename), "../infrastructures/connections/local.yml")
	}

	result, err := ioutil.ReadFile(filepath)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(result, env)
	if err != nil {
		return err
	}

	return nil
}

// GetEnvFlag func to get command line flag "env"
func GetEnvFlag(name, value, usage string) string {
	var flagEnv string
	flag.StringVar(&flagEnv, name, value, usage)
	flag.Parse()
	return flagEnv
}

// built-in function which start before the main function
func init() {
	// get env stage from command line flags
	stage := GetEnvFlag("env", "", "env Mode project local, development, staging, production")

	// get env stage config from enviroment variable, default local
	if utils.RemoveWhiteSpace(stage) == "" {
		stage = utils.GetEnv("ENV", "local")
	}

	// get file config base on stage
	err := GetConfigFile(&EnvConfig, stage)
	if err != nil {
		LogApp.Warningln(err)
	}

	// set stage Env
	EnvConfig.App.Environment = stage
	// check app config environment variable
	EnvConfig.App.Name = utils.GetEnv("APPNAME", EnvConfig.App.Name)
	EnvConfig.App.Debug = utils.GetEnv("APPDEBUG", EnvConfig.App.Debug)
	EnvConfig.App.Host = utils.GetEnv("APPHOST", EnvConfig.App.Host)
	EnvConfig.App.Port = utils.GetEnv("APPPORT", EnvConfig.App.Port)

	// check db config environment variable
	EnvConfig.SQLDatabase.Name = utils.GetEnv("SQLDBNAME", EnvConfig.SQLDatabase.Name)
	EnvConfig.SQLDatabase.User = utils.GetEnv("SQLDBUSER", EnvConfig.SQLDatabase.User)
	EnvConfig.SQLDatabase.Password = utils.GetEnv("SQLDBPASSWORD", EnvConfig.SQLDatabase.Password)
	EnvConfig.SQLDatabase.Port = utils.GetEnv("SQLDBPORT", EnvConfig.SQLDatabase.Port)
	EnvConfig.SQLDatabase.Connection = utils.GetEnv("SQLDBCONNECTION", EnvConfig.SQLDatabase.Connection)
	EnvConfig.SQLDatabase.Host = utils.GetEnv("SQLDBHOST", EnvConfig.SQLDatabase.Host)

	//check newrelic config environment variable
	EnvConfig.Elastic.Host = utils.GetEnv("ELASTICHOSTS", EnvConfig.Elastic.Host)

	//check keycloak config environment variable
	EnvConfig.KeyCloak.BaseAuthUrl = utils.GetEnv("KEYCLOAKBASEAUTHURL", EnvConfig.KeyCloak.BaseAuthUrl)

	//set stage debug status
	SetDebugStatus(EnvConfig.App.Debug, EnvConfig.App.Environment)
}
