package config

import (
	"database/sql"
	"fmt"
	"go.elastic.co/apm/module/apmsql"
	_ "go.elastic.co/apm/module/apmsql/pq"
	"cpe-go-repo/constants"
)

// GlobalDbSQL is a global variable to handle sql database connection
var GlobalDbSQL *sql.DB

// InitDb is a func to initialize sql database connection
func InitDb(dbConfig SQLDatabase) (con *sql.DB, err error) {
	connInfo := fmt.Sprintf(`%s:%s@tcp(%s:%s)/%s`, dbConfig.User, dbConfig.Password,
		dbConfig.Host, dbConfig.Port, dbConfig.Name)
	switch dbConfig.Connection {
	case "postgres":
		connInfo = fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable", dbConfig.Host, dbConfig.Port, dbConfig.User, dbConfig.Name, dbConfig.Password)
		break
	}

	con, err = apmsql.Open(dbConfig.Connection, connInfo)
	if err != nil {
		LogApp.Panic(err)
	}

	err = con.Ping()
	if err != nil {
		LogApp.Panic(err)
	}

	// db connection config
	con.SetMaxOpenConns(constants.MaxOpenConnection)
	con.SetMaxIdleConns(constants.MaxIdleConnection)
	con.SetConnMaxLifetime(constants.MaximumLifeTime)

	return con, err
}

// built-in function which start before the main function
func init() {
	GlobalDbSQL, _ = InitDb(EnvConfig.SQLDatabase)
}
