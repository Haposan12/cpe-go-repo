package config

import (
	"net/http"

	//used for side effects
	_ "net/http/pprof"
)

const (
	FilepathPem string = "../infrastructures/certificates/tunaiku-client.crt" // FilepathPem is a global const storing https certificate crt file
	FilePathKey string = "../infrastructures/certificates/tunaiku-client.key" // FilePathKey is a global const storing https certificate key file
)

var (
	Debug   string = "true"  // Debug global variable
	TypeENV string = "local" // TypeENV global variable
)

// SetDebugStatus util function to flag the development stage
func SetDebugStatus(debug string, stage string) {
	Debug = debug
	TypeENV = stage
}

// GetDebugStatus util function to enable or disable debug
func GetDebugStatus() bool {
	if Debug == "false" || TypeENV == "production" {
		return false
	}
	return true
}

// IsProduction util function to get dev stage
func IsProduction() bool {
	return TypeENV == "production"
}

// GetErrMessage to get error, replaced by optional message
func GetErrMessage(err error, optionalMessage string) string {
	if !GetDebugStatus() {
		return optionalMessage
	}
	return err.Error()
}

// GetMissingParams to get missing parameter in a request
func GetMissingParams(params []string, splitter string) string {
	if !GetDebugStatus() {
		return ""
	}
	result := ""
	for i := 0; i < len(params); i++ {
		if i < len(params)-1 {
			result += "<" + params[i] + ">" + ", "
		} else {
			result += "<" + params[i] + ">"
		}
	}
	return splitter + result + " missing"
}

// Listen is a func to start http server
func Listen(addr string, handler http.Handler) {
	var err error

	if IsProduction() {
		err = http.ListenAndServeTLS(addr, FilepathPem, FilePathKey, handler)
	} else {
		go func() {
			LogApp.Info(http.ListenAndServe("localhost:6060", nil))
		}()
		err = http.ListenAndServe(addr, handler)
	}
	if err != nil {
		LogApp.Fatal(err)
	}
}
