package config

import (
	"github.com/sirupsen/logrus"
)

var LogApp = logrus.New()

func Log(a ...interface{})  {
	if !GetDebugStatus(){
		return
	}
	LogApp.Debug(a...)
}

func Logln(a ...interface{})  {
	if !GetDebugStatus(){
		return
	}
	LogApp.Debug(a...)
}

func LogF(format string, a ...interface{})  {
	if !GetDebugStatus(){
		return
	}
	LogApp.Debugf(format, a...)
}

func init()  {
	LogApp.Formatter = new(logrus.TextFormatter)
}