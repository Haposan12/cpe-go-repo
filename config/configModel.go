package config

// Environment to store the config
type Environment struct {
	App         App
	SQLDatabase SQLDatabase
	Elastic		Elastic
	KeyCloak	KeyCloak
}

// App to store App config
type App struct {
	Name        string
	Environment string
	Debug       string
	Host        string
	Port        string
}

// SQLDatabase to store Sql database config
type SQLDatabase struct {
	Name       string
	User       string
	Password   string
	Port       string
	Connection string
	Host       string
}

// Elastic model
type Elastic struct {
	Host string 	`yaml:"host:omitempty"`
}

//KeyCloak model
type KeyCloak struct {
	BaseAuthUrl	string	`yaml:"baseAuthUrl"`
}
