package config

import (
	"context"
	elastic "gopkg.in/olivere/elastic.v7"
)

var ElasticAppClient *elastic.Client

// built-in function which start before the main function
func init() {
	var err error
	ctx := context.Background()

	ElasticAppClient, err = elastic.NewSimpleClient(elastic.SetURL(EnvConfig.Elastic.Host))
	if err != nil {
		LogApp.Panic(err)
	}
	info, code, err := ElasticAppClient.Ping(EnvConfig.Elastic.Host).Do(ctx)
	if err != nil {
		LogApp.Panic(err)
	}

	LogApp.Printf("Elasticsearch returned with code %d and version %s \n", code, info.Version.Number)
}
