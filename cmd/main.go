package main

import (
	"cpe-go-repo/config"
	"cpe-go-repo/routes"
	"runtime"
)

func main(){
	runtime.GOMAXPROCS(runtime.NumCPU())
	config.Logln("run on " + config.EnvConfig.App.Environment)
	config.Listen(config.EnvConfig.App.Host + ":" + config.EnvConfig.App.Port, routes.NewRoutes())
}