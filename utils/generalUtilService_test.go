package utils

import (
	"testing"
)

func TestReverse(t *testing.T) {
	var tests = []struct {
		input    string
		expected string
	}{
		{"hello", "olleh"},
		{"bob", "bob"},
		{"marvin", "nivram"},
	}

	for _, test := range tests {
		output := Reverse(test.input)
		if output != test.expected {
			t.Errorf("\033[31mReverse String was incorrect, expected %s, got %s\033[0m", test.expected, output)
		}
	}
}

func TestSubString(t *testing.T) {
	var tests = []struct {
		message  string
		start    int
		end      int
		expected string
	}{
		{"hello", 0, 1, "h"},
		{"marvin", 1, 4, "arv"},
		{"hello world", 2, 8, "llo wo"},
		{"aaf4c61ddcc5e8a2dabede0f3b482cd9aea9434d", 0, 12, "aaf4c61ddcc5"},
	}

	for _, test := range tests {
		output := SubString(test.message, test.start, test.end)
		if output != test.expected {
			t.Errorf("\033[31mSubString was incorrect, expected %s, got %s\033[0m", test.expected, output)
		}
	}
}

func TestSanitize(t *testing.T) {
	var tests = []struct {
		word     string
		cutset   string
		expected string
	}{
		{"marvin ", "", "marvin"},
		{"111111111bob1", "1", "bob"},
		{"12marvin12", "12", "marvin"},
		{"    12marvin12mitchell12", "12", "marvin12mitchell"},
		{"<iframe>test</iframe>", "<iframe></iframe>", "test"},
	}
	for _, test := range tests {
		output := Sanitize(test.word, test.cutset)
		if output != test.expected {
			t.Errorf("\033[31mSanitize String was incorrect, expected %s, got %s\033[0m", test.expected, output)
		}
	}
}

func TestClamp(t *testing.T) {
	var tests = []struct {
		lowBoundary  int
		highBoundary int
		value        int
		expected     int
	}{
		{1, 2, 2, 2},
		{100, 200, 220, 200},
		{-1, 1, 2, 1},
		{-1, 1, -2, -1},
		{-1000, 1000, 500, 500},
	}
	for _, test := range tests {
		output := Clamp(test.lowBoundary, test.highBoundary, test.value)
		if output != test.expected {
			t.Errorf("\033[31mClamp was incorrect, expected %d, got %d\033[0m", test.expected, output)
		}
	}
}

func TestPadLeft(t *testing.T) {
	var tests = []struct {
		str      string
		pad      string
		length   int
		expected string
	}{
		{"abcd", "0", 10, "000000abcd"},
		{"abcd", "0x", 10, "0x0x0xabcd"},
		{"abcd", "0", 4, "abcd"},
		{"abcd", "0xa", 4, "abcd"},
		{"abcd", "0xa", 3, "bcd"},
	}
	for _, test := range tests {
		output := PadLeft(test.str, test.pad, test.length)
		if output != test.expected {
			t.Errorf("\033[31mPad Left was incorrect, expected %s, got %s\033[0m", test.expected, output)
		}
	}
}

func TestPadRight(t *testing.T) {
	var tests = []struct {
		str      string
		pad      string
		length   int
		expected string
	}{
		{"abcd", "0", 10, "abcd000000"},
		{"abcd", "0x", 10, "abcd0x0x0x"},
		{"abcd", "0", 4, "abcd"},
		{"abcd", "0xa", 4, "abcd"},
		{"abcd", "0xa", 3, "abc"},
	}
	for _, test := range tests {
		output := PadRight(test.str, test.pad, test.length)
		if output != test.expected {
			t.Errorf("\033[31mPad Right was incorrect, expected %s, got %s\033[0m", test.expected, output)
		}
	}
}

func TestRemovePadLeft(t *testing.T) {
	var tests = []struct {
		str      string
		remove   string
		expected string
	}{
		{"00000abcd", "0", "abcd"},
		{"1212abcd", "12", "abcd"},
		{"0a0a0a0aabcd0a0a", "0a0a", "bcd0a0a"},
		{"      Marvin Mitchell", " ", "Marvin Mitchell"},
		{"  0    0Marvin Mitchell", "0 ", "Marvin Mitchell"},
	}
	for _, test := range tests {
		output := RemovePadLeft(test.str, test.remove)
		if output != test.expected {
			t.Errorf("\033[31mRemove Pad Left was incorrect, expected %s, got %s\033[0m", test.expected, output)
		}
	}
}

func TestRemovePadRight(t *testing.T) {
	var tests = []struct {
		str      string
		remove   string
		expected string
	}{
		{"00000abcd0000", "0", "00000abcd"},
		{"abcd1212", "12", "abcd"},
		{"0a0aabcd0a0a", "0a0a", "0a0aabcd"},
		{"Marvin Mitchell      ", " ", "Marvin Mitchell"},
		{"Marvin Mitchell 0  0  ", " 0", "Marvin Mitchell"},
	}
	for _, test := range tests {
		output := RemovePadRight(test.str, test.remove)
		if output != test.expected {
			t.Errorf("\033[31mRemove Pad Right was incorrect, expected %s, got %s\033[0m", test.expected, output)
		}
	}
}
