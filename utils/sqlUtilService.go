package utils

import "database/sql"

// Query struct to store multiple queries
type Query struct {
	Statement string
	Args      []interface{}
}

// QueryOne util func to run sql queries with
func QueryOne(sqlDB *sql.DB, query Query) (row *sql.Row, err error) {
	row = sqlDB.QueryRow(query.Statement, query.Args...)
	return
}
