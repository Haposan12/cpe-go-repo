package utils

import (
	"math/rand"
	"strings"
	"time"
)

// RemoveWhiteSpace util function
func RemoveWhiteSpace(word string) string {
	return strings.TrimSpace(word)
}

// Sanitize util function
func Sanitize(word string, cutset string) string {
	return strings.Trim(RemoveWhiteSpace(word), cutset)
}

// Reverse string
func Reverse(input string) string {
	runes := []rune(input)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

// SubString util func
func SubString(message string, start int, end int) string {
	return message[start:end]
}

// Clamp util function to get value between boundaries
func Clamp(lowBoundary int, highBoundary int, value int) int {
	if value < lowBoundary {
		return lowBoundary
	} else if value > highBoundary {
		return highBoundary
	}
	return value
}

// PadLeft pad left side of words
func PadLeft(str string, pad string, length int) string {
	for {
		str = pad + str
		if len(str) > length {
			return str[len(str)-length : len(str)]
		}
	}
}

// PadRight pad right side of words
func PadRight(str string, pad string, length int) string {
	for {
		str = str + pad
		if len(str) > length {
			return str[0:length]
		}
	}
}

// RemovePadLeft remove left trailing character
func RemovePadLeft(str string, remove string) string {
	return strings.TrimLeft(str, remove)
}

// RemovePadRight remove right trailing character
func RemovePadRight(str string, remove string) string {
	return strings.TrimRight(str, remove)
}

// GetRandomInt util function
func GetRandomInt(start int, end int) int {
	rand.Seed(time.Now().UnixNano())
	min := start
	max := end
	return rand.Intn((max - min) + min)
}
