package utils

import "testing"

func TestEncryptSha(t *testing.T) {
	var tests = []struct{
		message 	string
		shaType 	ShaType
		expected	string
	}{
		{"hello", 1, "aaf4c61ddcc5e8a2dabede0f3b482cd9aea9434d"},
		{"12345", 1, "8cb2237d0679ca88db6464eac60da96345513964"},
		{"hello123", 1, "4233137d1c510f2e55ba5cb220b864b11033f156"},
		{"hello", 256, "2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824"},
		{"12345", 256, "5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5"},
		{"hello123", 256, "27cc6994fc1c01ce6659c6bddca9b69c4c6a9418065e612c69d110b3f7b11f8a"},
	}

	for _, test :=  range tests{
		output := EncryptSha(test.message, test.shaType)
		if output != test.expected{
			t.Errorf("\033[31mEncryptSha%d was incorrect, expected %s, got %s\033[0m", test.shaType, test.expected, output)
		}
	}
}