package utils

import (
	"net"
	"net/http"
	"time"
)

var netTransport = &http.Transport{
	Dial: (&net.Dialer{
		Timeout:   10 * time.Second,
		KeepAlive: 30 * time.Second,
	}).Dial,
	TLSHandshakeTimeout: 10 * time.Second,
	MaxIdleConns:        100,
	MaxIdleConnsPerHost: 100,
}
var netClient = &http.Client{
	Timeout:   30 * time.Second,
	Transport: netTransport,
}

// Request function to do http request, default 30 second timeout
func Request(request *http.Request) (*http.Response, error) {
	netClient.Timeout = 30 * time.Second
	resp, err := netClient.Do(request)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

// RequestWithTimeout to do http request with user defined timeout
func RequestWithTimeout(request *http.Request, timeoutInSecond int) (*http.Response, error) {
	netClient.Timeout = time.Duration(timeoutInSecond) * time.Second
	resp, err := netClient.Do(request)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
