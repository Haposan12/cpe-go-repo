package utils

import "os"

// GetEnv util to get env variable
func GetEnv(name string, initialValue string) string {
	res := os.Getenv(name)
	if res != "" {
		return res
	}
	return initialValue
}
