package utils

import (
	"crypto/sha1"
	"crypto/sha256"
	"encoding/hex"
	"hash"
)

// ShaType is a type to store sha encryption method
type ShaType int

const (
	// Sha1 type
	Sha1 ShaType = 1
	// Sha256 type
	Sha256 ShaType = 256
)

// EncryptSha util function to encrpty sha
func EncryptSha(message string, shaType ShaType) string {
	var sha hash.Hash
	switch shaType {
	case Sha256:
		sha = sha256.New()
		break
	default:
		sha = sha1.New()
		break
	}
	sha.Write([]byte(message))
	//The argument to `Sum` can be used to append
	digest := sha.Sum(nil)

	return hex.EncodeToString(digest)
}
