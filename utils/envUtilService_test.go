package utils

import "testing"

func TestGetEnv(t *testing.T) {
	var tests = []struct{
		name			string
		initialValue	string
		expected		string
	}{
		{"a", "b", "b"},
	}

	for _, test := range tests{
		output := GetEnv(test.name, test.initialValue)
		if output != test.expected{
			t.Errorf("\033[31mGetEnv was incorrect, expected %s, got %s\033[0m", test.expected, output)
		}
	}
}
