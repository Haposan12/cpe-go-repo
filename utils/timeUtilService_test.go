package utils

import (
	"testing"
	"time"
)

func TestGetHourDifference(t *testing.T) {
	var tests = []struct {
		now      time.Time
		then     time.Time
		expected int
	}{
		{GetTime(1), GetTime(2), -1},
		{GetTime(1), GetTime(-2), 3},
		{GetTime(7 + 1), GetTime(-22), 30},
	}

	for _, test := range tests {
		output := GetHourDifference(test.now, test.then)
		if output != test.expected {
			t.Errorf("\033[31mGetHourDifference() was incorrect, expected %d, got %d\033[0m", test.expected, output)
		}
	}
}

func TestGetMinuteDifference(t *testing.T) {
	var tests = []struct {
		now      time.Time
		then     time.Time
		expected int
	}{
		{GetTime(1), GetTime(2), -1 * 60},
		{GetTime(1), GetTime(-2), 3 * 60},
		{GetTime(7 + 1), GetTime(-22), 30 * 60},
	}

	for _, test := range tests {
		output := GetMinuteDifference(test.now, test.then)
		if output != test.expected {
			t.Errorf("\033[31mGetMinuteDifference() was incorrect, expected %d, got %d\033[0m", test.expected, output)
		}
	}
}

func TestGetSecondDifference(t *testing.T) {
	var tests = []struct {
		now      time.Time
		then     time.Time
		expected int
	}{
		{GetTime(1), GetTime(2), -1 * 3600},
		{GetTime(1), GetTime(-2), 3 * 3600},
		{GetTime(7 + 1), GetTime(-22), 30 * 3600},
	}

	for _, test := range tests {
		output := GetSecondDifference(test.now, test.then)
		if output != test.expected {
			t.Errorf("\033[GetSecondDifference() was incorrect, expected %d, got %d\033[0m", test.expected, output)
		}
	}
}
