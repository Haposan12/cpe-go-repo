package utils

import (
	"math"
	"time"
)

// Now is a func to get current time string
func Now(hourOffset int, format string) string {
	return time.Now().UTC().Add(time.Hour * time.Duration(hourOffset)).Format(format)
}

// GetNanoSecond get current time in nano second format
func GetNanoSecond(hourOffset int) int64 {
	return time.Now().UTC().Add(time.Hour * time.Duration(hourOffset)).UnixNano()
}

// GetTime util function
func GetTime(hourOffset int) time.Time {
	return time.Now().UTC().Add(time.Hour * time.Duration(hourOffset))
}

// GetSpecificTime util function
func GetSpecificTime(hourOffset int, minuteOffset int, secondOffset int) time.Time {
	return time.Now().UTC().Add(time.Hour * time.Duration(hourOffset)).Add(time.Minute * time.Duration(minuteOffset)).Add(time.Second * time.Duration(secondOffset))
}

// GetTimeDifference util function
func GetTimeDifference(now time.Time, then time.Time) time.Duration {
	return now.Sub(then)
}

// GetHourDifference util function
func GetHourDifference(now time.Time, then time.Time) int {
	return int(math.Round(now.Sub(then).Hours()))
}

// GetMinuteDifference util function
func GetMinuteDifference(now time.Time, then time.Time) int {
	return int(math.Round(now.Sub(then).Minutes()))
}

// GetSecondDifference util function
func GetSecondDifference(now time.Time, then time.Time) int {
	return int(math.Round(now.Sub(then).Seconds()))
}

// StringToTime util function
func StringToTime(timeString string) time.Time {
	resultDate, err := time.Parse("2006-01-02 03:04:05", timeString)
	if err != nil {
		resultDate = time.Date(1900, 1, 1, 1, 1, 1, 1, time.UTC)
	}
	return resultDate
}
