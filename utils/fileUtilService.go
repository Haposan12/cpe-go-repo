package utils

import "io/ioutil"

// ReadFileAsString util function to read file and return it as string
func ReadFileAsString(filename string) (string, error) {
	dat, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	return string(dat), nil
}
