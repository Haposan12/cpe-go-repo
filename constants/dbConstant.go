package constants

const (
	MaxIdleConnection 	= 0 	// MaxIdleConnection const
	MaxOpenConnection 	= 100	// MaxOpenConnection const
	MaximumLifeTime 	= 0		// MaximumLifeTime const
)