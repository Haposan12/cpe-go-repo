package constants

const (
	// ErrorMessage500 is the default internal error message
	ErrorMessage500 = "Unexpected server error"
	// ErrorMessage403 default forbiden error message
	ErrorMessage403 = "Forbidden"
	// ErrorMessage400 default bad request error message
	ErrorMessage400 = "Bad request"
	//ErrorMessage404 default not found error message
	ErrorMessage404 = "Data not found"
)
