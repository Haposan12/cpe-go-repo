package constants

import "time"

const (
	TimeFormat      string        = "2006-01-02 15:04:05" // TimeFormat is a const for default time format
	TimeGmt         int           = 7                     // TimeGmt is a alias for GMT(+7)
	TimeFormatRfc   string        = time.RFC3339          // TimeFormatRfc is a standard time format based on RFC document
	TimeoutDuration time.Duration = 30 * time.Second      // TimeoutDuration default for server
	MaxRequest      int           = 100                   // Max Request at same time
)
