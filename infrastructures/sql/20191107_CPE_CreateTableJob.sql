create table jobs.job_category (
	id serial primary key,
	category_name character varying(100),
	active integer default 1,
	created_at timestamp without time zone default now()
);


create table jobs.job_listing (
	id serial primary key,
	job_category_id integer,
	job_title character varying(100),
	job_description text,
	job_function character varying(50),
	job_type integer default 1,
	total_vacancies integer,
	active integer default 1,
	created_at timestamp without time zone default now()
);