create table public.master_media (
	id serial primary key,
	name character varying(255),
	asset_root_link character varying(255),
	asset_media_link character varying(255),
	created_at timestamp without time zone default now(),
	active int default 1
)