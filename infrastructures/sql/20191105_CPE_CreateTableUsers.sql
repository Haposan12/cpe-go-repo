create table users.users (
	id serial primary key,
	username character varying(50) unique,
	password character varying(255),
	created_at timestamp without time zone default now(),
	usertype character varying(25) default  'CPE',
	role character varying(10),
	full_name character varying(100)
)