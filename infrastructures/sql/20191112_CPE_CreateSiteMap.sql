create table public.sitemap (
	id serial primary key,
	location character varying(255),
	last_modified timestamp without time zone default now(),
	changefreq character varying(10),
	priority real
)