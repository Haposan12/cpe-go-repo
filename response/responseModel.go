package response

//ResponseStruct model response
type ResponseStruct struct {
	Message string		`json:"message"`
	Data	interface{}	`json:"data,omitempty"`
	Time 	string
}

//ErrorStruct model response
type ErrorStruct struct {
	Code 		int
	Message 	string
}