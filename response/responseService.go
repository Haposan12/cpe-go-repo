package response

import (
	"cpe-go-repo/config"
	"cpe-go-repo/constants"
	"cpe-go-repo/utils"
	"encoding/json"
	"net/http"
)

//func HTTPResponse(w http.ResponseWriter, data interface{},message string ,statusCode int, uri string, timeIn string, body io.ReadCloser, requestMethod string)  {
//	var resultBody interface{}
//	_ = json.NewDecoder(body).Decode(&resultBody)
//	if resultBody == nil{
//		resultBody = ""
//	}
//	now := utils.Now(constants.TimeGmt, constants.TimeFormat)
//	responseTemplate := ResponseStruct{
//		Message: message,
//		Data: data,
//		Time: utils.Now(constants.TimeGmt, constants.TimeFormat),
//	}
//	w.Header().Set("Content-Type", "application.json")
//	w.Header().Set("Access-Control-Allow-Origin", "*")
//	w.WriteHeader(statusCode)
//	resp, err := json.Marshal(responseTemplate)
//	if err != nil{
//		config.Logln(err)
//	}
//	_, _ = w.Write(resp)
//	_ = config.NewRelicApp.RecordCustomEvent("log_events_response",
//		map[string]interface{}{
//			"requestHttp":   uri,
//			"requestMethod": requestMethod,
//			"requestBody":   resultBody,
//			"response":      string(resp),
//			"httpCode":      statusCode,
//			"timeIn":        timeIn,
//			"timeOut":       now,
//		})
//}

func HTTPResponse(w http.ResponseWriter, message string, data interface{}, statusCode int)  {
	responseTemplate := ResponseStruct{
		Message: message,
		Data:    data,
		Time:    utils.Now(constants.TimeGmt, constants.TimeFormat),
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	resp, err := json.Marshal(responseTemplate)
	if err != nil {
		config.Logln(err)
	}
	w.Write(resp)
}