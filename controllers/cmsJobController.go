package controllers

import (
	"cpe-go-repo/config"
	"cpe-go-repo/constants"
	"cpe-go-repo/repository/job"
	"cpe-go-repo/request"
	"cpe-go-repo/response"
	"database/sql"
	"encoding/json"
	"github.com/go-chi/chi"
	"go.elastic.co/apm"
	"net/http"
)

func InsertJobCategories(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("POST /cms/job/categories", "request")
	defer tx.End()
	//now := utils.Now(constants.TimeGmt, constants.TimeFormat)

	request := request.InsertJobCategoriesRequest{}

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}
	if request.Name == ""{
		response.HTTPResponse(w, "Missing required body parameter(s)", nil, http.StatusBadRequest)
		return
	}

	data, err := job.InsertJobCategory(r.Context(), request)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}
	response.HTTPResponse(w, "OK", data, http.StatusOK)
}

func GetJobCategories(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("GET /cms/job/categories", "request")
	defer tx.End()
	//now := utils.Now(constants.TimeGmt, constants.TimeFormat)

	data, err := job.GetJobCategories(r.Context())
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	if len(data) == 0{
		response.HTTPResponse(w, "Data not found", nil, http.StatusNotFound)
		return
	}
	response.HTTPResponse(w, "OK", data, http.StatusOK)
}

func InsertJobListing(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("POST /cms/job/listings", "request")
	defer tx.End()
	//now := utils.Now(constants.TimeGmt, constants.TimeFormat)
	request := request.InsertJobListingRequest{}

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}
	if request.ID == "" && request.Tittle == "" && request.Description == "" && request.Function == ""{
		response.HTTPResponse(w, "Missing required body parameter(s)", nil, http.StatusBadRequest)
		return
	}

	data, err := job.InsertJobListing(r.Context(), request)
	if err != nil && err != sql.ErrNoRows{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}
	if err == sql.ErrNoRows{
		response.HTTPResponse(w, "Data not found", nil, http.StatusNotFound)
		return
	}
	response.HTTPResponse(w, "OK", data, http.StatusOK)
}

func GetJobListing(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("GET /cms/job/listings", "request")
	defer tx.End()
	cat_id := r.URL.Query()["cat_id"]
	if len(cat_id) == 0{
		cat_id = append(cat_id,"0")
	}
	status := r.URL.Query()["status"]
	if len(status) == 0{
		status = append(status,"active")
	}
	//now := utils.Now(constants.TimeGmt, constants.TimeFormat)

	data, err := job.GetJobListing(r.Context(), cat_id[0], status[0])
	if err != nil && err != sql.ErrNoRows{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}
	if err == sql.ErrNoRows{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, "Data not found", nil, http.StatusNotFound)
		return
	}

	response.HTTPResponse(w, "OK", data, http.StatusOK)
}

func UpdateJobListing(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("PUT /cms/job/listings/{id}", "request")
	defer tx.End()
	request := request.UpdateJobListingRequest{}
	request.ID = chi.URLParam(r, "id")

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	if request.CategoryID == "" && request.Title == "" && request.Description == "" && request.JobFunction == ""{
		response.HTTPResponse(w, "Missing required parameter(s)", nil, http.StatusBadRequest)
		return
	}

	data, err := job.UpdateJobListing(r.Context(), request)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	response.HTTPResponse(w, "OK", data, http.StatusOK)
}

func GetJobDetail(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("GET /cms/job/listings/{id}", "request")
	defer tx.End()
	jobID := chi.URLParam(r, "id")

	data, err := job.GetJobDetail(r.Context(), jobID)
	if err != nil && err != sql.ErrNoRows{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	response.HTTPResponse(w, "OK", data, http.StatusOK)
	return
}

//func GetApplicantList(w http.ResponseWriter, r *http.Request)  {
//	job_id := chi.URLParam(r, "id")
//	job_type := r.URL.Query()["type"]
//
//	if job_id == ""{
//		response.HTTPResponse(w, "Missing required parameter(s)", nil, http.StatusBadRequest)
//		return
//	}
//
//	data, err := job.GetApplicantList(r.Context(), job_id, job_type[0])
//
//}