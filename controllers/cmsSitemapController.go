package controllers

import (
	"cpe-go-repo/config"
	"cpe-go-repo/constants"
	siteprovider "cpe-go-repo/repository/sitemap"
	request "cpe-go-repo/request"
	"cpe-go-repo/response"
	"cpe-go-repo/utils"
	"database/sql"
	"encoding/json"
	"go.elastic.co/apm"
	"net/http"
)

func GetSiteMap(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("GET /cms/sitemap", "request")
	defer tx.End()
	data, err := siteprovider.GetSiteMap(r.Context())
	if err != nil && err != sql.ErrNoRows{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	if len(data) == 0{
		response.HTTPResponse(w, "Data not found", nil, http.StatusNotFound)
		return
	}

	response.HTTPResponse(w, "OK", data, http.StatusOK)
}

func InsertSiteMap(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("POST /cms/sitemap", "request")
	defer tx.End()
	request := request.InsertSiteMapRequest{}

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	if request.Location == "" || request.ChangeFreq == "" || request.Priority == 0{
		response.HTTPResponse(w, "Missing required parameter(s)", nil, http.StatusBadRequest)
		return
	}

	data, err := siteprovider.InsertSiteMap(r.Context(), request)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	response.HTTPResponse(w, "OK", data, http.StatusOK)
}

func UpdateSiteMap(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("PUT /cms/sitemap", "request")
	defer tx.End()
	request := request.UpdateSiteMapRequest{}

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	if request.Location == "" || request.ChangeFreq == "" || request.Priority == 0{
		response.HTTPResponse(w, "Missing required parameter(s)", nil, http.StatusBadRequest)
		return
	}
	request.LastModified = utils.Now(constants.TimeGmt, constants.TimeFormat)

	data, err := siteprovider.UpdateSiteMap(r.Context(), request)
	if err != nil && err != sql.ErrNoRows{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	response.HTTPResponse(w, "OK", data, http.StatusOK)
}

func DeleteSiteMap(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("DELETE /cms/sitemap", "request")
	defer tx.End()
	id := r.URL.Query()["id"]
	if len(id) == 0 {
		response.HTTPResponse(w, "Missing required parameter(s)", nil, http.StatusBadRequest)
		return
	}

	err := siteprovider.DeleteSiteMap(r.Context(), id[0])
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	response.HTTPResponse(w, "OK", nil, http.StatusOK)
}