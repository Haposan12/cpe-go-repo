package controllers

import (
	"cpe-go-repo/config"
	"cpe-go-repo/constants"
	"cpe-go-repo/repository/cms"
	request "cpe-go-repo/request"
	"cpe-go-repo/response"
	"cpe-go-repo/utils"
	"database/sql"
	"encoding/json"
	"go.elastic.co/apm"
	"net/http"
)

//
//func InsertCmsUser(w http.ResponseWriter, r *http.Request)  {
//	data := &cms.CmsUser{}
//	data.Type = "CPE"
//	now := utils.Now(constants.TimeGmt, constants.TimeFormat)
//	err := json.NewDecoder(r.Body).Decode(data)
//
//	if err != nil{
//		response.HTTPResponse(w, nil, "Missing required parameter(s)", http.StatusBadRequest, r.RequestURI, now, r.Body, r.Method)
//		return
//	}
//
//	validateResult  := utils.ValidateUserForm(data, "insert")
//	if len(validateResult) != 0{
//		response.HTTPResponse(w, validateResult, "Bad Request", http.StatusBadRequest, r.RequestURI, now, r.Body, r.Method )
//		return
//	}
//	//token
//
//	validCSI := utils.ValidationRole(data.Role)
//	if validCSI{
//		//register into csi api
//	}
//
//
//
//	validLCT := utils.ValidationLCTRole(data.Role)
//	if validLCT{
//		data.Type = "AIM"
//	}
//
//	//encrypt password
//	data.Password = utils.EncryptSha(data.Password, 256)
//
//	result := cms2.Register(data)
//
//	if result != nil{
//		response.HTTPResponse(w, "Username already exist", "Bad Request", http.StatusBadRequest, r.RequestURI, now ,r.Body, r.Method)
//		return
//	}
//	response.HTTPResponse(w, "Success Insert Data", "OK", http.StatusOK, r.RequestURI, now ,r.Body, r.Method)
//	return
//}
//
func CmsUserLogin(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("POST /cms/user/login", "request")
	defer tx.End()
	//now := utils.Now(constants.TimeGmt, constants.TimeFormat)
	request := request.LoginCmsUserRequest{}
	err := json.NewDecoder(r.Body).Decode(&request)

	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	if request.Name == "" && request.Password == ""{
		response.HTTPResponse(w, "Missing required parameter(s)", nil, http.StatusBadRequest)
		return
	}
	//encrypt password
	request.Password = utils.EncryptSha(request.Password, 256)

	data, err := cms.CmsUserLogin(r.Context(), request.Name, request.Password)
	if err != nil && err != sql.ErrNoRows{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}
	if err == sql.ErrNoRows{

	}

	response.HTTPResponse(w, "OK", data, http.StatusOK)

}

//func GetCmsUser(w http.ResponseWriter, r *http.Request)  {
//	//just send a auth token -> get username, password, and position(usertype)
//	//data := &cms.CmsUser{}
//	ctx := r.Context()
//
//	now := utils.Now(constants.TimeGmt, constants.TimeFormat)
//	//err := json.NewDecoder(r.Body).Decode(data)
//
//	//if err != nil{
//	//	response.HTTPResponse(w, "Bad Request", "Missing required parameter(s)", http.StatusBadRequest, r.RequestURI, now, r.Body, r.Method)
//	//	return
//	//}
//	//data.Type = strings.ToLower(data.Type)
//	datas, error := cms2.GetUsers(ctx)
//
//	if error != nil{
//		panic(error)
//	}
//	response.HTTPResponse(w, datas,"OK" ,http.StatusOK, r.RequestURI, now, r.Body, r.Method)
//	return
//}
//
//func CmsUserUpdateController(w http.ResponseWriter, r *http.Request)  {
//
//}

func GetCountDashboardAdmin(w http.ResponseWriter, r *http.Request)  {
	
}
