package controllers

import (
	"context"
	"cpe-go-repo/config"
	"cpe-go-repo/constants"
	contentprovider "cpe-go-repo/repository/cmsContent"
	request "cpe-go-repo/request"
	"cpe-go-repo/response"
	"encoding/json"
	"github.com/go-chi/chi"
	"go.elastic.co/apm"
	"net/http"
)

var mapping = `
{
	"settings":{
		"number_of_shards": 1,
		"number_of_replicas": 0
	},
	"mappings":{
			"properties":{
				"type":{
					"type":"text"
				},
				"title":{
					"type":"text"
				},
				"description":{
					"type":"text"
				},
				"url":{
					"type":"keyword"
				}
			}
	}
}`


func AddContent(w http.ResponseWriter, r *http.Request)  {
	//check index content exist or not in elasticsearch
	ctx := context.Background()
	exists, err := config.ElasticAppClient.IndexExists("content").Do(ctx)
	if err != nil{
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}
	if !exists{
		//create a new index
		newIndex, err := config.ElasticAppClient.CreateIndex("content").BodyString(mapping).Do(ctx)
		if err != nil{
			panic(err)
		}
		if !newIndex.Acknowledged{
			//not acknowledged
		}
	}

	request := request.InsertContentRequest{}
	err = json.NewDecoder(r.Body).Decode(&request)
	if err != nil{
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	//insert content into index
	_, err = config.ElasticAppClient.Index().Index("content").BodyJson(request).Do(ctx)
	if err != nil{
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500),nil, http.StatusInternalServerError)
		return
	}
	//insert content to db internal
	err = contentprovider.InsertContent(r.Context(), request)
	if err != nil{
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	response.HTTPResponse(w, "OK", nil, http.StatusOK)
}

func GetContent(w http.ResponseWriter, r *http.Request)  {
	content_type := r.URL.Query()["type"]
	//content_title := r.URL.Query()["title"]

	if content_type[0] == ""{
		response.HTTPResponse(w, "Missing required parameter(s)", nil, http.StatusInternalServerError)
		return
	}

}

func DeleteContent(w http.ResponseWriter, r *http.Request){
	tx := apm.DefaultTracer.StartTransaction("DELETE /cms/content/{id}", "request")
	defer tx.End()
	id := chi.URLParam(r, "id")

	err := contentprovider.DeleteContent(r.Context(), id)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	response.HTTPResponse(w, "OK", nil, http.StatusOK)
}
