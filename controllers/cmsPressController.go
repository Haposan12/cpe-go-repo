package controllers

import (
	"go.elastic.co/apm"
	"cpe-go-repo/config"
	"cpe-go-repo/constants"
	pressProvider "cpe-go-repo/repository/press"
	request "cpe-go-repo/request"
	"cpe-go-repo/response"
	"cpe-go-repo/utils"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

)

func InsertPressMedia(w http.ResponseWriter, r *http.Request)  {
	//name := r.FormValue("name")
	file, handler, err := r.FormFile("image")

	defer file.Close()
	if err != nil{
		panic(err)
	}
	bytes, err := ioutil.ReadAll(file)
	if err != nil{
		panic(err)
	}
	fmt.Println(handler.Filename, bytes)
}

func GetPressMedia(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("GET /cms/press_media", "request")
	defer tx.End()
	//now := utils.Now(constants.TimeGmt, constants.TimeFormat)
	data, err := pressProvider.GetPressMedia(r.Context())
	if err != nil && err != sql.ErrNoRows{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	if len(data) == 0{
		response.HTTPResponse(w, "Data not found", nil, http.StatusNotFound)
		return
	}

	response.HTTPResponse(w, "OK", data, http.StatusOK)
}

func DeletePressMedia(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("DELETE /cms/press_media", "request")
	defer tx.End()
	id := r.URL.Query()["id"]

	if len(id) == 0{
		response.HTTPResponse(w, "Missing required parameter(s)", nil, http.StatusBadRequest)
		return
	}

	err := pressProvider.DeletePressMedia(r.Context(), id[0])
	if err != nil{
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	response.HTTPResponse(w, "OK", nil, http.StatusOK)
}

func InsertPressArticle(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("POST /cms/press_article", "request")
	defer tx.End()
	request := request.InsertPressArticleRequest{}

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	if request.Title == "" || request.Link == "" || request.Published == ""{
		response.HTTPResponse(w, "Missing required parameter(s)", nil, http.StatusBadRequest)
		return
	}

	data, err := pressProvider.InsertPressArticle(r.Context(), request)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	response.HTTPResponse(w, "OK", data, http.StatusOK)
}

func GetPressArticle(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("GET /cms/press_article", "request")
	defer tx.End()
	limit := r.URL.Query()["limit"]
	if len(limit) == 0{
		limit = append(limit,"20")
	}
	offset := r.URL.Query()["offset"]
	if len(offset) == 0{
		offset = append(offset,"0")
	}
	showType := r.URL.Query()["show"]
	if len(showType) == 0{
		showType = append(showType,"limit")
	}
	//now := utils.Now(constants.TimeGmt, constants.TimeFormat)

	data, err := pressProvider.GetPressArticle(r.Context(), utils.StringToInt(limit[0]), utils.StringToInt(offset[0]), showType[0])

	if err != nil && err != sql.ErrNoRows{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	if len(data) == 0{
		response.HTTPResponse(w, "Data not found", nil, http.StatusNotFound)
		return
	}

	response.HTTPResponse(w, "OK", data, http.StatusOK)
}

func DeletePressArticle(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("DELETE /cms/press_article", "request")
	defer tx.End()
	id := r.URL.Query()["id"]
	if len(id) == 0{
		response.HTTPResponse(w, "Missing required parameter(s)", nil, http.StatusBadRequest)
		return
	}

	err := pressProvider.DeletePressArticle(r.Context(), id[0])
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	response.HTTPResponse(w, "OK", nil, http.StatusOK)
}

func UpdatePressArticle(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("PUT /cms/press_article", "request")
	defer tx.End()
	request := request.UpdatePressArticleRequest{}

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	if request.Title == "" || request.Link == "" || request.Published == ""{
		response.HTTPResponse(w, "Missing required parameter(s)", nil, http.StatusBadRequest)
		return
	}

	data, err := pressProvider.UpdatePressArticle(r.Context(), request)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	response.HTTPResponse(w, "OK", data, http.StatusOK)
}

func GetPressRelease(w http.ResponseWriter, r *http.Request){
	tx := apm.DefaultTracer.StartTransaction("GET /press_release", "request")
	defer tx.End()
	limit := r.URL.Query()["limit"]
	if len(limit) == 0{
		limit = append(limit,"10")
	}
	offset := r.URL.Query()["offset"]
	if len(offset) == 0{
		offset = append(offset,"0")
	}
	showType := r.URL.Query()["show"]
	if len(showType) == 0{
		showType = append(showType,"limit")
	}
	//now := utils.Now(constants.TimeGmt, constants.TimeFormat)

	data, err := pressProvider.GetPressRelease(r.Context(), utils.StringToInt(limit[0]), utils.StringToInt(offset[0]), showType[0])

	if err != nil && err != sql.ErrNoRows{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	if len(data) == 0{
		response.HTTPResponse(w, "Data not found", nil, http.StatusNotFound)
		return
	}

	response.HTTPResponse(w, "OK", data, http.StatusOK)

}

func DeletePressRelease(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("DELETE /cms/press_release", "request")
	defer tx.End()
	id := r.URL.Query()["id"]
	if len(id) == 0{
		response.HTTPResponse(w, "Missing required parameter(s)", nil, http.StatusBadRequest)
		return
	}

	err := pressProvider.DeletePressRelease(r.Context(), id[0])
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}

	response.HTTPResponse(w, "OK", nil, http.StatusOK)
}

func InsertPressRelease(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("POST  /cms/press_release", "request")
	defer tx.End()
	request := request.InsertPressReleaseRequest{}

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}
	if request.Title == "" && request.Body == ""{
		response.HTTPResponse(w, "Missing required parameter(s)", nil, http.StatusBadRequest)
		return
	}

	err = pressProvider.InsertPressRelease(r.Context(), request)
	if err != nil{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}
	response.HTTPResponse(w, "OK", nil, http.StatusOK)

}

func GetDetailPressRelease(w http.ResponseWriter, r *http.Request)  {
	tx := apm.DefaultTracer.StartTransaction("GET /cms/press_release/view", "request")
	defer tx.End()
	//now := utils.Now(constants.TimeGmt, constants.TimeFormat)
	id, ok := r.URL.Query()["id"]
	if !ok{
		response.HTTPResponse(w,  "Missing required parameter(s)", nil, http.StatusBadRequest)
		return
	}

	data, err := pressProvider.GetDetailPressRelease(r.Context(), id[0])
	if err != nil && err != sql.ErrNoRows{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage500), nil, http.StatusInternalServerError)
		return
	}
	if err == sql.ErrNoRows{
		config.LogApp.Warningln(err)
		response.HTTPResponse(w, config.GetErrMessage(err, constants.ErrorMessage400), nil, http.StatusNotFound)
		return
	}


	response.HTTPResponse(w, "OK", data, http.StatusOK)
}