package controllers

import (
	"cpe-go-repo/config"
	"cpe-go-repo/response"
	"net/http"
)

func Ping(w http.ResponseWriter, r *http.Request)  {
	ctx := r.Context()


	_, err := config.GlobalDbSQL.ExecContext(ctx, "select notification_id, pg_sleep(2) from notification.details limit 1")
	if err != nil{
		println(err.Error())
		return
	}

	response.HTTPResponse(w, "ok","OK", http.StatusOK)
	return
}
