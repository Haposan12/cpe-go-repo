package request

type InsertContentRequest struct {
	ContentType		string		`json:"type"`
	Title			string		`json:"title"`
	Description		string		`json:"description"`
	Url 			string		`json:"url"`
}
