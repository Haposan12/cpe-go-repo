package request

type InsertJobCategoriesRequest struct{
	Name 		string	`json:"category_name"`
}

type InsertJobListingRequest struct {
	ID 			string	`json:"id"`
	Tittle		string	`json:"tittle"`
	Description string	`json:"description"`
	Function	string	`json:"job_function"`
	Type 		string	`json:"job_type"`
	Vacancies	int		`json:"total_vacancies"`
}

type UpdateJobListingRequest struct {
	ID 			string 	`json:"id"`
	CategoryID	string	`json:"category_id"`
	Title		string	`json:"title"`
	Description	string	`json:"description"`
	JobFunction	string	`json:"job_function"`
	JobType		string	`json:"type"`
	Vacancies	int		`json:"total_vacancies"`
	Status		int		`json:"active"`
}