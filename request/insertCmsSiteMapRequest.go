package request

type InsertSiteMapRequest struct {
	Location 	string		`json:"location"`
	ChangeFreq	string		`json:"changefreq"`
	Priority	float32		`json:"priority"`
}

type UpdateSiteMapRequest struct {
	Location 	 string		`json:"location"`
	ChangeFreq	 string		`json:"changefreq"`
	Priority	 float32	`json:"priority"`
	LastModified string		`json:"lastmodified"`
	ID			 string		`json:"id"`
}
