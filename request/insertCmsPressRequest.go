package request

type InsertPressMediaRequest struct {
	Name 	string		`json:"name"`
	image 	string		`json:"image"`
}

type InsertPressArticleRequest struct {
	Title 		string 		`json:"title"`
	Link 		string		`json:"link"`
	Published	string		`json:"published"`
	MediaID 	int			`json:"media_id"`
}

type InsertPressReleaseRequest struct {
	Title 		string 		`json:"title"`
	Body		string		`json:"body"`
	User  		string		`json:"username"`
}

type UpdatePressArticleRequest struct {
	ID 			int			`json:"id"`
	Title 		string 		`json:"title"`
	Link 		string		`json:"link"`
	Published	string		`json:"published"`
	MediaID 	int			`json:"media_id"`
} 

