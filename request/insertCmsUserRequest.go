package request

type LoginCmsUserRequest struct {
	Name 		string		`json:"username"`
	Password	string		`json:"password"`
}
